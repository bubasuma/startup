<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user \frontend\models\User */

$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['user/confirm-email', 'token' => $user->email_confirmation_token]);
?>
<div class="password-reset">
    <p>Hello <?=$user->profile->fullName?>,</p>

    <p>Follow the link below to confirm your email:</p>

    <p><?= Html::a(Html::encode($resetLink), $resetLink) ?></p>
</div>