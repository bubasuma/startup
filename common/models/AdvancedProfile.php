<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "advanced_profile".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $first_name
 * @property string $last_name
 * @property string $gender
 * @property string $profession
 * @property string $created_at
 * @property string $updated_at
 */
class AdvancedProfile extends SimpleProfile
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'advanced_profile';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'created_at'], 'required'],
            [['user_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['first_name', 'last_name', 'profession'], 'string', 'max' => 30]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'profession' => 'Profession',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
