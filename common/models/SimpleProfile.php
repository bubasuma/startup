<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "simple_profile".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $first_name
 * @property string $last_name
 * @property string $gender
 * @property string $created_at
 * @property string $updated_at
 *
 *
 * @property string $fullName
 */
class SimpleProfile extends ActiveRecord
{
    const GENDER_FEMALE = 'F';
    const GENDER_MALE   = 'M';
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'simple_profile';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'created_at'], 'required'],
            [['user_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe']
        ];
    }

    public function getFullName(){
        return trim($this->first_name.' '.$this->last_name);
    }



    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
