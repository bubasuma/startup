<?php
namespace console\migrations;

abstract class Migration extends \yii\db\Migration {

    const TABLE_USER                = '{{%user}}';
    const TABLE_SIMPLE_PROFILE      = '{{%simple_profile}}';
    const TABLE_ADVANCED_PROFILE    = '{{%advanced_profile}}';
    const TABLE_AUTH    = '{{%auth}}';


    public function getTableOptions(){
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        return $tableOptions;
    }

    public function addForeignKey($name, $table, $columns, $refTable, $refColumns, $delete = null, $update = null)
    {
        if(null === $name){
            $altColumns = $columns;
            if(is_array($altColumns)){
                $altColumns = implode('-',$altColumns);
            }

            $name = 'fk-'.trim($table,'{}%').'-'.$altColumns;
        }
        parent::addForeignKey($name, $table, $columns, $refTable, $refColumns, $delete, $update);
    }


}