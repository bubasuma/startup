<?php



class m150416_165137_advanced_profile_table extends console\migrations\Migration
{
    public function up()
    {

        $this->createTable(self::TABLE_ADVANCED_PROFILE, [
            'id'                        => 'INT UNSIGNED AUTO_INCREMENT PRIMARY KEY',
            'user_id'                   => 'INT UNSIGNED NOT NULL',
            'first_name'                => 'VARCHAR(30) DEFAULT NULL',
            'last_name'                 => 'VARCHAR(30) DEFAULT NULL',
            'gender'                    => 'CHAR(1) DEFAULT NULL',
            'profession'                => 'VARCHAR(30) DEFAULT NULL',
            'created_at'                => 'DATETIME NOT NULL',
            'updated_at'                => 'DATETIME DEFAULT NULL',
        ], $this->tableOptions);

        $this->addForeignKey(null,self::TABLE_ADVANCED_PROFILE,'user_id',self::TABLE_USER,'id','CASCADE','CASCADE');
    }


    public function down()
    {
        $this->dropTable(self::TABLE_ADVANCED_PROFILE);
    }
}
