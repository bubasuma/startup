<?php
namespace frontend\models;

use common\models\SimpleProfile;
use yii\base\Model;
use Yii;

/**
 * Sig nup form
 */
abstract class SignUpForm extends Model
{
    public $username;
    public $email;
    public $password;
    public $confirmPassword;

    //optional fields
    public $firstName ;
    public $lastName ;
    public $gender ;

    //auth fields
    public $sourceId;
    public $sourceName;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['username', 'filter', 'filter' =>function($value){return strtolower(trim(strip_tags($value)));}],
            ['username', 'required'],
            ['username', 'unique', 'targetClass' => '\frontend\models\User', 'message' => 'This username has already been taken.'],
            ['username', 'string', 'min' => 2, 'max' => 255],

            [['firstName','lastName'], 'filter', 'filter' =>function($value){return ucfirst(trim(strip_tags($value)));}],
            [['firstName','lastName'], 'string', 'min' => 2, 'max' => 255],

            ['gender', 'filter', 'filter' =>function($value){return strtoupper(trim(strip_tags($value)));}],
            ['gender','in','range'=>[SimpleProfile::GENDER_FEMALE,SimpleProfile::GENDER_MALE]],

            ['sourceId', 'match','pattern'=>'/^[a-z0-9A-Z_]+$/'],
            ['sourceName', 'in','range'=>['google','facebook','twitter']],
            [['sourceName', 'sourceId'], 'string', 'max' => 255],

            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'unique', 'targetClass' => '\frontend\models\User', 'message' => 'This email address has already been taken.'],

            ['password', 'required'],
            ['password','compare','compareAttribute'=>'username','operator'=>'!=','message'=>'You are using your username as password'],
            ['password','compare','compareAttribute'=>'email','operator'=>'!=','message'=>'You are using your email as password'],
            ['password', 'match','pattern'=>'/^(.){8,20}$/'],
            ['password', 'string', 'min' => 6],

            ['confirmPassword','compare','compareAttribute'=>'password','message'=>'You must exactly repeat the password picked  above']
        ];
    }



    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    abstract  public function signUp();




}
