<?php
/**
 * Created by PhpStorm.
 * User: buba
 * Date: 16.04.15
 * Time: 19:06
 */

namespace frontend\models;


use common\models\Auth;
use common\models\SimpleProfile;
use yii\base\Exception;

class SimpleSignUpForm extends SignUpForm {

    public function signUp()
    {

        if ($this->validate()) {
            $tr = \Yii::$app->db->beginTransaction();
            try {
                $user = new User();
                $user->username = $this->username;
                $user->email = $this->email;
                $user->setPassword($this->password);
                $user->generateAuthKey();
                if ($user->save()) {
                    $user->profile = new SimpleProfile();
                    $user->profile->user_id    = $user->id;
                    $user->profile->first_name = $this->firstName?:null;
                    $user->profile->last_name = $this->lastName?:null;
                    $user->profile->gender = $this->gender?:null;
                    if ($user->profile->save()) {
                        if($this->sourceId && $this->sourceName){
                            $auth = new Auth([
                                'user_id' => $user->id,
                                'source' => $this->sourceName,
                                'source_id' => $this->sourceId,
                            ]);
                            if (!$auth->save()) {
                                throw   new Exception(json_encode($auth->getErrors()));
                            }
                        }

                        $tr->commit();
                        $user->sendEmailConfirmation();
                        return $user;

                    } else {
                        throw   new Exception(json_encode($user->profile->getErrors()));
                    }
                } else {
                    throw   new Exception(json_encode($user->getErrors()));
                }
            } catch (\Exception $ex){
                $tr->rollBack();
                \Yii::error($ex->getMessage(),__METHOD__);
            }

        }

        return null;
    }

}