<?php
/**
 * Created by PhpStorm.
 * User: buba
 * Date: 09.04.15
 * Time: 19:28
 */

namespace frontend\models;


class User extends \common\models\User{


    /**
     * @inheritdoc
     */
    public static function find()
    {
        return parent::find()->andWhere(['role'=>self::ROLE_USER]);
    }

    public function afterSave($insert, $changedAttributes)
    {
        if($insert){
            if($this->role === self::ROLE_USER){
                //welcome message to simple user
            }else if ($this->role === self::ROLE_ADVANCED){
                //welcome message to advanced user
            }
        }
        parent::afterSave($insert, $changedAttributes);
    }

    /**
     * @param string $str
     * @return string|null
     */
    public static function parseGender($str){
        if(is_string($str) && !empty($str)){
            return strtoupper(substr($str,0,1));
        }else{
            return null;
        }
    }


}