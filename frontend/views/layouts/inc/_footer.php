<?php
/**
 * Created by PhpStorm.
 * User: buba
 * Date: 27.04.15
 * Time: 20:51
 */
?>
<footer id="footer-bar" class="row">
    <p id="footer-copyright" class="col-xs-12">
        &copy; 2014 <a href="http://www.adbee.sk/" target="_blank">Adbee digital</a>. Powered by Centaurus Theme.
    </p>
</footer>