<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\AdvancedSignUpForm */

$this->title = 'Signup';
$this->params['breadcrumbs'][] = $this->title;

$script=<<<INLINE
    document.addEventListener('auth', function (e) {

        $.each(e.detail, function(k, v) {
            $('#form-sign-up [name*='+k+']').val(v);
        });
    }, false);
INLINE;

$this->registerJs($script);

?>
<div class="site-signup">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>Please fill out the following fields to signup:</p>

    <div class="row">
        <div class="col-lg-5">
            <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>
                <?= $form->field($model, 'username') ?>
                <?= $form->field($model, 'email') ?>
                <?= $form->field($model, 'password')->passwordInput() ?>
                <?= $form->field($model, 'confirmPassword')->passwordInput() ?>
                <?= $form->field($model, 'profession') ?>
                <div class="form-group">
                    <?= Html::submitButton('Sign up', ['class' => 'btn btn-primary', 'name' => 'sign-up-button']) ?>
                </div>
            <?php ActiveForm::end(); ?>
        </div>
        <div class="col-lg-5">
            <?= \yii\authclient\widgets\AuthChoice::widget([
                'baseAuthUrl' => ['site/auth'],
                'popupMode' => true,
            ]) ?>
        </div>
    </div>
</div>
